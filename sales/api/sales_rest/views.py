from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .models import Sale, AutomobileVO, Employee, Customer
from common.json import ModelEncoder
from django.http import JsonResponse
# Create your views here.

class EmployeeDetailEncoder(ModelEncoder):
    model = Employee
    properties = [
        "name",
        "employee_id",
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "employee",
        "customer",
        "automobile",
        "sale_price",
        "id",
    ]
    encoders = {
        "employee": EmployeeDetailEncoder(),
        "customer": CustomerDetailEncoder(),
        "automobile": AutomobileVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_sales(request, automobile_vo_id=None):

    if request.method == "GET":
        if automobile_vo_id != None:
            sales = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sales = Sale.objects.all()
            print(sales)
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,

        )
    else:
        content = json.loads(request.body)

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )

        try:
            customer_href = content["customer_id"]
            customer = Customer.objects.get(id=customer_href)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        try:
            employee_href = content["employee_id"]
            employee = Employee.objects.get(employee_id=employee_href)
            content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )

        sales =Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleListEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_employees(request):
    if request.method == "GET":
        employees = Employee.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder = EmployeeDetailEncoder
        )
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder=EmployeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerDetailEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT",])
def api_show_sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "employee" in content:
                employee = Employee.objects.get(employee_id=content["employee"])
                content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
            )
        Sale.objects.filter(id=pk).update(**content)
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
