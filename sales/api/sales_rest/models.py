from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField(primary_key=True)

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveIntegerField()

class AutomobileVO(models.Model):
    vin = models.CharField(primary_key=True, max_length=17, unique=True)

class Sale(models.Model):
    employee = models.ForeignKey(
        Employee,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True,
    )

    sale_price = models.FloatField()
