from django.urls import path
from .views import api_list_sales, api_list_employees, api_list_customers, api_show_sale

urlpatterns = [
    path("sales/<int:pk>/", api_show_sale, name="api_show_sale"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("employees/", api_list_employees, name="api_list_employees"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("automobiles/<int:automobile_vo_id>/sales/", api_list_sales, name="api_list_sales"),
]
