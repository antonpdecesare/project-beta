
from django.urls import path
from .views import list_appointments, list_techs, show_appointments

urlpatterns = [
    path("appointments/", list_appointments, name="list_appointments"),
    path("technicians/", list_techs, name="list_techs"),
    path("appointments/<int:pk>/", show_appointments, name="show_appointments"),
]
