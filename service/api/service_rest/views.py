from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from common.json import ModelEncoder
from django.shortcuts import render
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "id",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "technician_name",
        "employee_number",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model=Appointment
    properties=[
        "customer_name",
        "date",
        "time",
        "reason",
        "vip_status",
        "appointment_status",
        "id",
        "vin",
        "technician",
    ]
    encoders={
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_techs(request):
    if request.method=="GET":
        technician=Technician.objects.all()
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianListEncoder
        )
    else:
        try:
            content=json.loads(request.body)
            technician=Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Try again for Technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_appointments(request, automobile_vin=None):
    if request.method=="GET":
        if automobile_vin==None:
            appointments=Appointment.objects.all()
        else:
            vin=automobile_vin
            appointments=Appointment.objects.filter(vin=vin)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            technician=content["technician"]
            technician=Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
            {'message': "Invalid technician"},
            status=404,
        )
        vin=content["vin"]
        autos=AutomobileVO.objects.all()
        automobile_vin=[]
        for auto in autos:
            automobile_vin.append(auto.vin)
        if vin in automobile_vin:
            content["vip_status"]=True
        appointment=Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_appointments(request, pk):
    if request.method=="GET":
        try:
            appointment=Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response=JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method=="DELETE":
        try:
            appointment=Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:
        try:
            content=json.loads(request.body)
            Appointment.objects.filter(id=pk).update(**content)
            appointment=Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response=JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
