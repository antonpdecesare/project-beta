# CarCar

Team:

* Person 1 - Service - Anton
* Person 2 - Sales - Ben

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

My models were created based off the needs of the microservice. For technicians we needed to have a name and employee number to keep track of each individual technician. The appointment model was created using some information provided however some fields were added for functionality. For example, vip_status is a Boolean field because it is either true or not true.  An AutomobileVO was used to represent a first-class object. This with the immutabililty and safety from corruption of data makes a VO very useful. It also allows the user to communicate between the microservice and the inventory.

The integration with the inventory started with my views. In my views I wrote REST API's to communicate to the inventory. We used "GET", "POST", "PUT", and "DELETE" methods to create information that the inventory can access. I was able to confirm that the calls were successful by checking endpoints in Insomnia for each API call method.


There are ways to view technicians, and appointments. The technicians list shows all fields in its model case, being technician name and employee number. The applications list takes data which matches the forms. It also has the ability to create a technician as well as mark an appointment as complete, or cancel the appointment which deletes the appointment from the database. It also has the ability to to search for appointment and filter a result.
## Sales microservice

For the sales microservice, in order to create and display a sale record, the sale model needed to pull data from the customer model, employees model, and the VehicleVO by using foreign keys. When a user fill outs the sales form on the front end it only lets them select an existing VIN that can be found in the automobile inventory because the VehicleVO is linked to the inventory microservice.
