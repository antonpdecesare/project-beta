import React, {useState, useEffect} from 'react';

function SalesForm() {
  const [automobiles, setAutomobiles] = useState([])
  const [employees, setEmployees] = useState([])
  const [customers, setCustomers] = useState([])


  const [formData, setFormData] = useState({
    automobile: '',
    employee_id: '',
    customer_id: '',
    sale_price: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);

    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const fetchEmployeeData = async () => {
    const url = 'http://localhost:8090/api/employees/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setEmployees(data.employees);

    }
  }

  useEffect(() => {
    fetchEmployeeData();
  }, []);

  const fetchCustomerData = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  useEffect(() => {
    fetchCustomerData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        automobile: '',
        employee_id: '',
        customer_id: '',
        sale_price: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;


    setFormData({
      ...formData,


      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Sale Record</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
          <div className="mb-3">
              <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an automobile</option>
                {(automobiles).map(automobile => {
                  return (
                    <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.employee_id} required name="employee_id" id="employee_id" className="form-select">
                <option value="">Choose an Employee</option>
                {(employees).map(employee => {
                  return (
                    <option key={employee.id} value={employee.employee_id}>{employee.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.customer_id} required name="customer_id" id="customer_id" className="form-select">
                <option value="">Choose a customer</option>
                {(customers).map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.sale_price} placeholder="Sale Price" required type="text" name="sale_price" id="sale_price" className="form-control" />
              <label htmlFor="sale_price">Sale Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
