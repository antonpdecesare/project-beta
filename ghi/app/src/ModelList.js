import React, { useEffect, useState } from 'react';

function ModelList() {
    const [models, setModels] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/models/");
        const data = await response.json();
        setModels(data.models);
    }

    useEffect(() => {
        getData()
    }, [])


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => (
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td><img src={model.picture_url} className="img-fluid"/></td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ModelList;
