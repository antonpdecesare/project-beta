import React, { useState, useEffect } from 'react';

function AppointmentTable() {
    const [appointments, setAppointments] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");
        const data = await response.json();
        setAppointments(data.appointments);
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}`, {
                method: "DELETE"
            });
            if(response.ok) {
                setAppointments(appointments.filter(appointment => appointment.id !== id));
            }
        } catch (error) {
            console.error(error);
        }
    }

    const handleRemove = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}`, {
                method: "PATCH"
            });
            if(response.ok) {
                setAppointments(appointments.filter(appointment => appointment.id !== id));
            }
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>VIN</th>
                    <th>Reason</th>
                    <th>VIP Status</th>
                    <th>Appointment Status</th>
                    <th>Technician</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => (
                    <tr key={appointment.vin}>
                        <td>{appointment.customer_name}</td>
                        <td>{appointment.date}</td>
                        <td>{appointment.time}</td>
                        <td>{appointment.vin}</td>
                        <td>{appointment.reason}</td>
                        <td>{appointment.vip_status ? 'Yes' : 'No'}</td>
                        <td>{appointment.appointment_status}</td>
                        <td>{appointment.technician.technician_name}</td>
                        <td>
                        <button className= "btn btn-danger" onClick={() => handleDelete(appointment.id)}>Cancel</button>
                        </td>
                        <td>
                            <button className="btn btn-success" onClick={() => handleRemove(appointment.id)}>Finished</button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default AppointmentTable;
