
import React, { useState, useEffect } from 'react';

function TechnicianTable() {
    const [technician, setTechnician] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/");
        const data = await response.json();
        setTechnician(data.technician);
    }

    useEffect(() => {
        getData()
    }, [])


    return (

        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>Technician Name</th>
                    <th>Employee Number</th>
                </tr>
            </thead>
            <tbody>
                {technician.map(technician => (
                    <tr key={technician.id}>
                        <td>{technician.technician_name}</td>
                        <td>{technician.employee_number}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        )}

export default TechnicianTable;
