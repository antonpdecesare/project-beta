import React, { useState, useEffect } from "react";


const SalesHistory = () => {
    const [filterValue, setFilterValue] = useState("");
    const [sales, setSales] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (e) => {
        setFilterValue(e.target.value.toLowerCase());
    };

    const filteredSales = (e) => {
        if (filterValue === "") {
            return [];
        } else {
            return sales.filter((sale) =>
                sale.employee["name"].toLowerCase().includes(filterValue)
            );
        };
    };
    return (
        <div className="container">
            <div className="form-floating mb-3">
                <input onChange={handleChange} placeholder="Search Sales Person" required type="text" name="filterValue" id="style" className="form-control" value={filterValue} button="Search"/>
                <button>Search for Sales Person</button>
            </div>
            <h1>Sales History</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>

                    </tr>
                </thead>
                <tbody>
                    {filteredSales().map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.employee.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.sale_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesHistory;
