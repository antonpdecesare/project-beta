import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import EmployeeForm from './EmployeeForm';
import AppointmentList from './AppointmentList';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import ManufacturerList from './ManufacturerList';
import ModelList from './ModelList';
import AutomobileList from './AutomobileList';
import ManufacturerForm from './ManufacturerForm';
import ModelForm from './ModelForm';
import CustomerForm from './CustomerForm';
import SalesHistory from './SalesHistory';
import AppointmentHistory from './AppointmentHistory';
import AutomobileForm from './AutomobileForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales" element={<SalesList/>} />
          <Route path="sales">
            <Route path="new" element={<SalesForm/>} />
          </Route>
          <Route path="technicians" element={<TechnicianList/>} />
          <Route path = "technicians">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments" element={<AppointmentList/>} />
          <Route path = "appointments">
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route path = "appointments">
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path = "manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" element={<ModelList/>} />
          <Route path = "models">
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles" element={<AutomobileList/>} />
          <Route path = "employees">
            <Route path="new" element={<EmployeeForm/>} />
          </Route>
          <Route path = "customers">
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path = "sales">
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path = "automobiles">
            <Route path="new" element={<AutomobileForm/>} />
          </Route>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
