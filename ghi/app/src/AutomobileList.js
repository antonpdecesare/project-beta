import React, { useState, useEffect } from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");
        const data = await response.json();
        setAutomobiles(data.autos);
    }

    useEffect(() => {
        getData()
    }, [])


    return (

        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>VIN</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Year</th>
                </tr>
            </thead>
            <tbody>
                {automobiles.map(automobile => (
                    <tr key={automobile.vin}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default AutomobileList;
