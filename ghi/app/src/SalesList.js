import React, { useEffect, useState } from 'react';

function SalesList() {

  const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        const data = await response.json();
        setSales(data.sales);
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee Name</th>
            <th>Employee ID</th>
            <th>Customer Name</th>
            <th>Automobile VIN</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
        {sales.map(sale => {
          return (
          <tr key={ sale.id }>
            <td>{ sale.employee.name }</td>
            <td>{ sale.employee.employee_id }</td>
            <td>{ sale.customer.name }</td>
            <td>{ sale.automobile.vin }</td>
            <td>{ sale.sale_price }</td>
          </tr>
        );
      })}
        </tbody>
      </table>
    )
}
    export default SalesList;
